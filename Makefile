all: pr-settingsmod

pr-settingsmod: pr-settingsmod.c
	$(CC) -o $@ $< `pkg-config --cflags --libs libconfig`

clean:
	rm -f pr-settingsmod.o pr-settingsmod

